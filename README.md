<h3>First setup (necessary):</h3>
- Edit the file <i>conf/database.php</i> according to your MySQL credentials.<br>
- Uncomment the line <code>extension=gd</code> inside the <i>php.ini</i> file on your system.<br>
- Ensure that a web server (like Apache) and MySQL are running.

<h3>Embed into your application (optional):</h3>
- MySQL tables can be edited inside the file <i>conf/tables.php</i>.<br>
- The title of the website can be changed inside <i>php/title.php</i>.<br>
- The icon of the website can be changed replacing <i>img/favicon.ico</i>.<br>
- A successful login will redirect to the <i>dashboard.php</i> page. Edit this last file according to the content of your web application.

<h3>Screenshots:</h3>
![Screenshot](screenshot1.png)
![Screenshot](screenshot2.png)
