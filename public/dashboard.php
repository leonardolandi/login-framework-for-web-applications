<!-- Start session -->
<?php session_start(); ?>

<?php

	// If someone tries to access this page without passing throught the login page
    if (!isset($_SESSION["username"])) {
		header("Location: ../login.php");
		exit;
	}
	
?>

<html>

	<head>
		<title>
			<?php
				include "php/title.php";
			?>
		</title>
		<link rel=shortcut icon href=img/favicon.ico>
	</head>

	<body>

		Welcome, <?php echo $_SESSION["username"]; ?> !
		
	</body>

</html>
