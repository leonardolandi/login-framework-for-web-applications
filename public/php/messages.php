<?php

	$MESSAGES = array(
		"UNKNOWN_ERROR" => "An unknown error occurred. Please try again",
		"INVALID_USERNAME_GENERIC" => "Please enter a valid Username",
		"INVALID_USERNAME_SHORT" => "Username is too short (min 3 characters)",
		"INVALID_USERNAME_LONG" => "Username is too long (max 30 characters)",
		"INVALID_USERNAME_CHARS" => "Username must contain only letters and numbers",
		"INVALID_USERNAME_EXISTS" => "Username already in use",
		"INVALID_PASSWORD_GENERIC" => "Please enter a valid Password",
		"INVALID_PASSWORD_SHORT" => "Password is too short (min 3 characters)",
		"INVALID_PASSWORD_LONG" => "Password is too long (max 30 characters)",
		"INVALID_PASSWORD_CHARS" => "Password must contain only letters and numbers",
		"INVALID_PASSWORD_MATCH" => "Passwords don't match",
		"INVALID_AUTH" => "Invalid Username or Password",
		"INVALID_CAPTCHA" => "Invalid captcha",
		"REGISTER_SUCCESS" => "Registration successful!"
	);
	
?>
