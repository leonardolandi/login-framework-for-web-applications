<?php

	// Start session
	session_start();

	// If someone tries to access this page without passing throught the captcha page
    if (!isset($_POST["captcha"],$_SESSION["newusername"],$_SESSION["newpassword"],$_SESSION["captcha"])) {
		header("Location: ../register.php");
		exit;
	}

	// Get form data
	$captcha = $_POST["captcha"];
    
	// Validate Captcha
    if ($captcha !== $_SESSION["captcha"]) {
    	$_SESSION["errorcode"] = "INVALID_CAPTCHA";
    	header("Location: ../confirm.php");
		exit;
	}
	
	// Connect to database
    require "connect.php";
    
	// Ask the database if the username already exists
	$result = $MYSQL->query("SELECT lowername FROM users WHERE lowername = '" . strtolower($_SESSION["newusername"]) . "'");
	
	// If username is already in use
	if ($result->num_rows > 0) {
		$_SESSION["errorcode"] = "INVALID_USERNAME_EXISTS";
		header("Location: ../register.php");
		exit;
	}
	
	// Add the new user to the database
	$result = $MYSQL->query(
		"INSERT INTO users SET 
			lowername = '" . strtolower($_SESSION["newusername"]) . "', 
			username = '" . $_SESSION["newusername"] . "', 
			password = '" . password_hash($_SESSION["newpassword"],PASSWORD_BCRYPT) . "'"
		);
	$MYSQL->close();
	
	// Set new session keys and unset the others
	$_SESSION["loginusername"] = $_SESSION["newusername"];
	$_SESSION["loginpassword"] = $_SESSION["newpassword"];
	$_SESSION["errorcode"] = "REGISTER_SUCCESS";
	unset($_SESSION["newusername"]);
	unset($_SESSION["newpassword"]);
	unset($_SESSION["captcha"]);
	
	// Redirect to the login page
	header("Location: ../login.php");
	exit;
		
?>
