<?php

	// Start session
	session_start();

	// If someone tries to access this page without passing throught the registration page
    if (!isset($_POST["username"],$_POST["password"],$_POST["repassword"])) {
		header("Location: ../register.php");
		exit;
	}

	// Get form data
	$username = $_POST["username"];
    $password = $_POST["password"];
    $repassword = $_POST["repassword"];
    
    // Save data in session
    $_SESSION["newusername"] = $username;
    $_SESSION["newpassword"] = $password;
    $_SESSION["renewpassword"] = $repassword;
    
	// Validate Username and Password
    
	// Username is non-string
	if (!is_string($username)) { $_SESSION["errorcode"] = "INVALID_USERNAME_GENERIC"; }
	// Username is empty
	elseif (strlen($username) == 0) { $_SESSION["errorcode"] = "INVALID_USERNAME_GENERIC"; }
	// Username is too short
	elseif (strlen($username) < 3) { $_SESSION["errorcode"] = "INVALID_USERNAME_SHORT"; }
	// Username is too long
	elseif (strlen($username) > 30) { $_SESSION["errorcode"] = "INVALID_USERNAME_LONG"; }
	// Username contains invalid characters
	elseif (preg_match("/[^A-Za-z0-9]/", $username)) { $_SESSION["errorcode"] = "INVALID_USERNAME_CHARS"; }
	// Password is non-string
	elseif (!is_string($password)) { $_SESSION["errorcode"] = "INVALID_PASSWORD_GENERIC"; }
	// Password is empty
	elseif (strlen($password) == 0) { $_SESSION["errorcode"] = "INVALID_PASSWORD_GENERIC"; }
	// Password is too short
	elseif (strlen($password) < 3) { $_SESSION["errorcode"] = "INVALID_PASSWORD_SHORT"; }
	// Password is too long
	elseif (strlen($password) > 30) { $_SESSION["errorcode"] = "INVALID_PASSWORD_LONG"; }
	// Password contains invalid characters
	elseif (preg_match("/[^A-Za-z0-9]/", $password)) { $_SESSION["errorcode"] = "INVALID_PASSWORD_CHARS"; }
	// Second password is non-string
	elseif (!is_string($repassword)) { $_SESSION["errorcode"] = "INVALID_PASSWORD_MATCH"; }
	// Second password does not match with the first
	elseif ($password !== $repassword) { $_SESSION["errorcode"] ="INVALID_PASSWORD_MATCH"; }
	
	// An error occurred during validation
	if (isset($_SESSION["errorcode"])) {
    	header("Location: ../register.php");
		exit;
	}
	
	// Connect to database
    require "connect.php";
    
	// Ask the database if the username already exists
	$result = $MYSQL->query("SELECT lowername FROM users WHERE lowername = '" . strtolower($username) . "'");
	$MYSQL->close();
	
	// If username is already in use
	if ($result->num_rows > 0) {
		$_SESSION["errorcode"] = "INVALID_USERNAME_EXISTS";
		header("Location: ../register.php");
		exit;
	}
	
	// Unset everything but username, password -->
	unset($_SESSION["errorcode"]);
	unset($_SESSION["renewpassword"]);
	
	// Redirect to the captcha validation page
	header("Location: ../confirm.php");
	exit;
		
?>
