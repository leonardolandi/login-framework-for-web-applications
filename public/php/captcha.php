<?php

	// Start session
	session_start();
	
	// If someone tries to access this page without passing throught the captcha page
	if (!isset($_SESSION["newusername"],$_SESSION["newpassword"])) {
		header("Location: ../register.php");
		exit;
	}
	
	// Store the value
	$text = "";
	$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	for ($i=0; $i<5; $i++) { $text .= $chars[rand(0,strlen($chars)-1)]; }
	$_SESSION["captcha"] = $text;
	
	// Create an image
	$image = imagecreate(240,60);
	
	// Set background color
	$backgroundColor = imagecolorallocate($image, 0, 0, 0);
    imagecolortransparent($image, $backgroundColor);

	// Add the text
	$textColor = imagecolorallocate($image, 0, 0, 0);
	$shadowColor = imagecolorallocate($image, 150, 150, 150);
	$font = "../font/Pangolin.ttf";
	for ($i=0; $i<5; $i++) {
		$size = rand(30,40);
		$angle = rand(-30,30);
		$base = rand(40,50);
		imagettftext($image, $size, $angle, (32+($i*40)), ($base+2), $shadowColor, $font, $text[$i]);
		imagettftext($image, $size, $angle, (30+($i*40)), $base, $textColor, $font, $text[$i]);
	}

	// Add some noise lines
	$linesColor = imagecolorallocate($image, 0, 0, 0);
	imagesetthickness($image, 1);
	for ($i=0; $i<2; $i++) {
		imageline($image, 0, rand(0,30), 240, rand(0,60), $linesColor);
		imageline($image, rand(0,240), 0, rand(0,240), 60, $linesColor);
	}
	
	
	// Send the image
	header("Content-type: image/png");
	imagepng($image);
	imagecolordeallocate($textColor);
	imagecolordeallocate($shadowColor);
	imagecolordeallocate($linesColor);
	imagecolordeallocate($backgroundColor);
	imagedestroy($image);
	
	exit;
	
?>
