<?php

	// Include the MySQL database configuration file
	require_once "../../conf/database.php";

	// Connect to database
	$MYSQL = new mysqli(
    	$DBHOST,
    	$DBUSER,
    	$DBPASS
    );
    
    // An error occurred during database connection
	if($MYSQL->connect_error){
		$_SESSION["errorcode"] = "UNKNOWN_ERROR";
		header("Location: ../index.php");
		exit;
	}
    
    // First setup: initialize database if not exists
    $db = $MYSQL->select_db($DBNAME);
    if ($db) { unset($db); }
    else {
    
    	// Include the tables configuration
		require_once "../../conf/tables.php";

		// Create database if it doesn't exist
		$createdb = $MYSQL->query("CREATE DATABASE IF NOT EXISTS " . $DBNAME);
		
		// Some error arose while creating the database
		if (!$createdb) {
			$_SESSION["errorcode"] = "UNKNOWN_ERROR";
			header("Location: ../index.php");
			exit;
		}
			
		// Create tables if they don't exists
		$MYSQL->select_db($DBNAME);
		foreach ($DBTABLES as $table => $fields) {
		
			// Create the query
			$str = "CREATE TABLE IF NOT EXISTS " . $table . " (";
			foreach ($fields as $field => $type) {
				$str .= $field . " " . $type . ", ";
			}
			$str = substr($str,0,-2) . ")";
			
			// Apply the query
			$createtable = $MYSQL->query($str);
			
			// Some error arose while creating the table
			if (!$createtable) {
				$_SESSION["errorcode"] = "UNKNOWN_ERROR";
				header("Location: ../index.php");
				exit;
			}
			
		}
		
	}
    
?>
