<?php

	// Start session
	session_start();
	
	// If someone tries to access this page without passing throught the login page
    if (!isset($_POST["username"],$_POST["password"])) {
		header("Location: ../login.php");
		exit;
	}

	// Get form data
	$username = $_POST["username"];
    $password = $_POST["password"];
    
    // Save data in session
    $_SESSION["loginusername"] = $username;
    $_SESSION["loginpassword"] = $password;
    
	// Validate Username and Password
	if (
		!is_string($username) ||					// Username is non-string
		strlen($username) < 3 ||					// Username is too short
		strlen($username) > 30 ||					// Username is too long
		preg_match("/[^A-Za-z0-9]/", $username)	||	// Username contains invalid characters
		!is_string($password) ||					// Password is non-string
		strlen($password) < 3 ||					// Password is too short
		strlen($password) > 30 ||					// Password is too long
		preg_match("/[^A-Za-z0-9]/", $password)		// Password contains invalid characters
	) {
		$_SESSION["errorcode"] = "INVALID_AUTH";
		header("Location: ../login.php");
		exit;
	}
	
	// Connect to database
    require "connect.php";
    
	// Ask the database if credentials are valid
	$result = $MYSQL->query("SELECT username, password FROM users WHERE lowername = '" . strtolower($username) . "'");
	$MYSQL->close();
	
	// Username doesn't exist
	if ($result->num_rows == 0) {
		$_SESSION["errorcode"] = "INVALID_AUTH";
		header("Location: ../login.php");
		exit;
	}
	// Username with that password doesn't exist
	else {
		$row = $result->fetch_assoc();
		if (!password_verify($password,$row["password"])) {
			$_SESSION["errorcode"] = "INVALID_AUTH";
			header("Location: ../login.php");
			exit;
		}
	}
    
    // Unset session
    session_unset();
    $_SESSION["username"] = $username;
    
    // Redirect to the dashboard
    header("Location: ../dashboard.php");
	exit;

?>
