function removeHighlight(dom) {
	
	if (dom.classList.contains("highlight")) {
		dom.classList.remove("highlight");
		dom.classList.add("nohighlight");
		document.getElementById("error").classList.remove("visible");
		document.getElementById("error").classList.add("hidden");
	}
	
}
