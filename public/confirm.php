<!-- Start session -->
<?php session_start(); ?>

<?php

	// If someone tries to access this page without registering before
	if (!isset($_SESSION["newusername"],$_SESSION["newpassword"])) {
		header("Location: register.php");
		exit;
	}
	
?>

<html>

	<head>
		<title>
			<?php
				include "php/title.php";
			?>
			- Confirm
		</title>
		<meta name=viewport content="width=device-width, initial-scale=1">
		<link rel=stylesheet type=text/css href=css/login.css>
		<link rel=shortcut icon href=img/favicon.ico>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
		<script src=js/removeHighlight.js></script>
	</head>

	<body>
	
		<div class=login>

			<!-- Captcha form -->
			<form action=php/validateCaptcha.php method=post>
				<br>
				<b>Confirm Registration</b>
				<br>
				<hr>
				Please confirm that you are a human
				<br>
				Write the text below
				<br><br>
				<div id=error
					<?php
						if (!isset($_SESSION["errorcode"])) { echo " class=hidden>"; }
						else {
							include_once "php/messages.php";
							echo " class=visible>" . $MESSAGES[$_SESSION["errorcode"]];
							unset($MESSAGES);
						}
					?>
					<br><br>
				</div>
				<img src=php/captcha.php />
				<br><br>
				<div class=divinput>
					<input type=text name=captcha oninput=removeHighlight(this)
						<?php
							if (isset($_SESSION["errorcode"]) && $_SESSION["errorcode"] === "INVALID_CAPTCHA") { echo " class=highlight"; }
							else { echo " class=nohighlight"; }
						?>
					autofocus />
					<a href=confirm.php alt="Reload Captcha"><i class="fa fa-sync-alt"></i></a>
				</div>
				<br><br>
				<input type=submit value=Confirm />
				<br><br>
				or <a href=login.php>Back to Login</a>
			</form>
		
		</div>
		
	</body>

</html>

<!-- Unset everything but newusername, newpassword, captcha -->
<?php unset($_SESSION["errorcode"]); ?>
