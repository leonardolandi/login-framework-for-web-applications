<!-- Start session -->
<?php session_start(); ?>

<html>

	<head>
		<title>
			<?php
				include "php/title.php";
			?>
			- Login
		</title>
		<meta name=viewport content="width=device-width, initial-scale=1">
		<link rel=stylesheet type=text/css href=css/login.css>
		<link rel=shortcut icon href=img/favicon.ico>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
		<script src=js/removeHighlight.js></script>
	</head>

	<body>

		<div class=login>
		
			<!-- Login form -->
			<form action=php/validateLogin.php method=post>
				<br>
				<b>Login</b>
				<br>
				<hr>
				<div id=error
					<?php
						if (!isset($_SESSION["errorcode"])) { echo " class=hidden>"; }
						elseif ($_SESSION["errorcode"] === "REGISTER_SUCCESS") {
							include_once "php/messages.php";
							echo " class=visible_green><br>" . $MESSAGES[$_SESSION["errorcode"]];
							unset($MESSAGES);
						}
						else {
							include_once "php/messages.php";
							echo " class=visible><br>" . $MESSAGES[$_SESSION["errorcode"]];
							unset($MESSAGES);
						}
					?>
				</div>
				<br>
				<div class=divinput>
					<i class="fa fa-user"></i>
					<input type=text name=username placeholder="Username" maxlength=30 oninput=removeHighlight(this)
						<?php
							if (isset($_SESSION["loginusername"])) { echo " value='" . $_SESSION["loginusername"] . "' "; }
						?>
					autofocus />
				</div>
				<br>
				<div class=divinput>
					<i class="fa fa-lock"></i>
					<input type=password name=password placeholder="Password" maxlength=30 oninput=removeHighlight(this)
						<?php
							if (isset($_SESSION["loginpassword"])) { echo " value='" . $_SESSION["loginpassword"] . "' "; }
						?>
					/>
				</div>
				<br><br>
				<input type=submit value=Enter />
				<br><br>
				or <a href=register.php>Create new account</a>
			</form>
		
		</div>
		
	</body>

</html>

<!-- Unset session -->
<?php session_unset(); ?>
