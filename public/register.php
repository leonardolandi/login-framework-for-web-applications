<!-- Start session -->
<?php session_start(); ?>

<html>

	<head>
		<title>
			<?php
				include "php/title.php";
			?>
			- New Account
		</title>
		<meta name=viewport content="width=device-width, initial-scale=1">
		<link rel=stylesheet type=text/css href=css/login.css>
		<link rel=shortcut icon href=img/favicon.ico>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
		<script src=js/removeHighlight.js></script>
	</head>

	<body>

		<div class=login>
		
			<!-- Create new account form -->
			<form action=php/validateNewAccount.php method=post>
				<br>
				<b>Create new account</b>
				<br>
				<hr>
				<div id=error
					<?php
						if (!isset($_SESSION["errorcode"])) { echo " class=hidden>"; }
						else {
							include_once "php/messages.php";
							echo " class=visible><br>" . $MESSAGES[$_SESSION["errorcode"]];
							unset($MESSAGES);
						}
					?>
				</div>
				<br>
				<div class=divinput>
					<i class="fa fa-user"></i>
					<input type=text name=username placeholder="Choose your new Username" maxlength=30 oninput=removeHighlight(this)
						<?php
							if (!isset($_SESSION["errorcode"])) { echo " autofocus"; }
							elseif (substr($_SESSION["errorcode"],0,16) === "INVALID_USERNAME") { echo " class=highlight autofocus"; }
							else { echo " class=nohighlight"; }
							
							if (isset($_SESSION["newusername"])) { echo " value='" . $_SESSION["newusername"] . "' "; }
						?>
					/>
				</div>
				<br>
				<div class=divinput>
					<i class="fa fa-lock"></i>
					<input type=password name=password placeholder="Choose your new Password" maxlength=30 oninput=removeHighlight(this)
						<?php
							if (isset($_SESSION["errorcode"])) {
								if (substr($_SESSION["errorcode"],0,16) === "INVALID_PASSWORD") { echo " class=highlight autofocus"; }
								else { echo " class=nohighlight"; }
							}
							
							if (isset($_SESSION["newpassword"])) { echo " value='" . $_SESSION["newpassword"] . "' "; }
						?>
					/>
				</div>
				<br>
				<div class=divinput>
					<i class="fa fa-lock"></i>
					<input type=password name=repassword placeholder="Reinsert your new Password" maxlength=30 oninput=removeHighlight(this)
						<?php
							if (isset($_SESSION["renewpassword"])) { echo " value='" . $_SESSION["renewpassword"] . "' "; }
						?>
					/>
				</div>
				<br><br>
				<input type=submit value=Confirm />
				<br><br>
				or <a href=login.php>Back to Login</a>
			</form>
		
		</div>
		
	</body>

</html>

<!-- Unset session -->
<?php session_unset(); ?>
