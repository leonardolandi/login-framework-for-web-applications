<!-- Start session -->
<?php session_start(); ?>

<?php

	// If the user is logged, go to the dashboard, else to the login page
	if (isset($_SESSION["username"])) {
		header("Location: dashboard.php");
		exit;
	}
	else {
		header("Location: login.php");
		exit;
	}

?>
